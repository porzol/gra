package PanstwaMiasta;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Player {
    private String name;
    private int points;
    private String answer;
    private boolean answerCorrect;

    public void setPoints(int points) {
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public String getAnswer() {
        return answer;
    }

    public boolean setAnswerCorrect() {
        return answerCorrect=false;
    }
    public boolean compareAnswersToTheList (String answer) throws FileNotFoundException {
        File tekst = new File("C:\\Users\\JAREK\\Projekty\\CitiesAndCountries\\src\\countries");
        Scanner scanner = new Scanner(tekst);
        boolean toReturn = false;
        while (scanner.hasNextLine() && !toReturn) {
            String line = scanner.nextLine();
            if (line.trim().equalsIgnoreCase(answer)) {
                System.out.println("great, it works");

                return toReturn=true;


            } else toReturn=false;

            }return toReturn;
        }



    public void setAnswerCorrect(boolean answerCorrect) {

        this.answerCorrect = answerCorrect;
    }

    public Player(String name, int points, String answer, boolean answerCorrect) {

        this.name = name;
        this.points = points;
        this.answer = answer;
        this.answerCorrect = answerCorrect;
    }

    public void giveCity (){
        System.out.println(getName()+" Wprowadz miasto: ");
        Scanner scanner = new Scanner(System.in);
        String answer = scanner.nextLine();
        setAnswer(answer);
    }


    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isAnswerCorrect() {
        return answerCorrect;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", points=" + points +
                ", answer='" + answer + '\'' +
                ", answerCorrect=" + answerCorrect +
                '}';
    }
}
